exports.users = [
  {
    name: 'Leon Gaban',
    email: 'quxquz@gmail.com'
  },
  {
    name: 'Juan Gonzales',
    email: 'foobaz@gmail.com'
  },
  {
    name: 'Foo Bar',
    email: 'foobar@gmail.com'
  }
]
