const MongoClient = require('mongodb').MongoClient;

// const { NODE_ENV, PORT, HOST, USER, PASS } = process.env

exports.start = async ({ NODE_ENV, PORT, HOST, USER, PASS }) => {
  const uri = `mongodb+srv://${USER}:${PASS}@${HOST}`

  console.log('env', NODE_ENV, PORT, HOST, USER, PASS)
  console.log('uri', uri)

  const client = new MongoClient(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })

  try {
    await client.connect(err => {
      const collection = client.db('moonholdings').collection('users')
      // perform actions on the collection object
      // console.log('collection', collection);
      client.close()
    });
  } catch(error) {
    console.log(error)
  }
}

exports.close = async () => {
  // client.close();
}

exports.query = async (q, data) => {
  
}
