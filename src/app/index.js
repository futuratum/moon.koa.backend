const Koa = require('koa')
const router = require('routing')
// const Router = require('@koa/router')
const Morgan = require('koa-morgan')
const server = new Koa()
const db = require('database')
require('dotenv').config()

const { NODE_ENV, PORT, HOST, USER, PASS } = process.env

exports.start = async () => {
  server
    .use(require('koa-body')())
    .use(Morgan('dev'))
    .use(router.allowedMethods())
    .use(router.routes())

  try {
    // console.log('process.env', process.env)
    await db.start(process.env)
    console.log('Database connected...')
    await server.listen(3000)
    console.log('env:', NODE_ENV, PORT, HOST, USER, PASS)
  } catch(error) {
    console.log('ERROR', error)
  }

  console.log('Koa server running on port 3000...')
}
